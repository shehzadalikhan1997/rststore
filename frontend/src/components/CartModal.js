import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  Grid,
  Flex,
  Image,
  Divider,
  Text,
  Heading,
} from "@chakra-ui/react";

import { Link as RouterLink, useNavigate } from "react-router-dom";
import { addToCart } from "../actions/cartAction";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

const CartModal = ({ qty, product, id }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  useEffect(() => {
    if (id) {
      dispatch(addToCart(id, +qty));
    }
  }, [dispatch, qty, id]);

  const addToCartHandler = () => {
    console.log("Clicked");
    onOpen();
    if (id) {
      dispatch(addToCart(qty, id));
    }
  };
  const goToCartHandler = () => {
    navigate(`/cart/${id}?qty=${qty}`);
  };
  return (
    <>
      <Button
        onClick={addToCartHandler}
        colorScheme="teal"
        my="2"
        textTransform="uppercase"
        letterSpacing="wide"
        isDisabled={product.countInStock === 0}
      >
        Add To Cart
      </Button>

      <Modal size="4xl" isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color="blue.500" fontSize="3xl" mt="5">
            Successfully Added to Cart !
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Grid
              templateColumns={{ base: "1fr", md: "4fr 2fr", lg: "6fr 3fr" }}
            >
              {/* Column 1 */}
              <Grid templateColumns="2fr 2fr" gap="2" justifyContent="center">
                <Image
                  w="250px"
                  h="300px"
                  src={product.image}
                  alt={product.name}
                />
                <Flex direction="column" mx="5">
                  <Heading my="3" as="h3" fontSize="2xl" color="gray.800">
                    {product.name}
                  </Heading>
                  <Text color="blue.600" fontSize="2xl" fontWeight="bold">
                    ₹{product.price}{" "}
                  </Text>
                  <Text fontSize="md">Quantity : {qty}</Text>
                  <br />
                  <Text fontSize="sm" textAlign="justify">
                    <strong>Product Description : </strong>{" "}
                    {product.description}
                  </Text>
                </Flex>
                <Divider
                  color="gray.800"
                  orientation={{
                    base: "horizontal",
                    lg: "vertical",
                    md: "vertical",
                  }}
                />
              </Grid>

              {/* Column 2 */}

              <Flex direction="column">
                <Flex direction="column" alignItems="center">
                  <Text
                    my="2"
                    mb="7"
                    fontSize="2xl"
                    color="gray.800"
                    textTransform="uppercase"
                    fontWeight="bold"
                    textColor="teal"
                  >
                    Your-Cart Details
                  </Text>
                </Flex>
                <Flex direction="row" justifyContent="space-between">
                  <Text>Quantity : </Text>
                  <Text>{qty} item(s)</Text>
                </Flex>
                <Flex my="2" direction="row" justifyContent="space-between">
                  <Text>Total Product Cost: </Text>
                  <Text>₹{qty * product.price}</Text>
                </Flex>
                <Flex my="2" direction="row" justifyContent="space-between">
                  <Text>Total Delivery Cost: </Text>
                  <Text>FREE</Text>
                </Flex>

                <Divider color="gray.800" />
                <Flex my="2" direction="row" justifyContent="space-between">
                  <Text>
                    Total Cost : <br />
                    (inclusive of all products){" "}
                  </Text>
                  <Text>
                    ₹
                    {cartItems.reduce(
                      (acc, currVal) =>
                        acc + (currVal.qty || 1) * +currVal.price,
                      0
                    )}
                  </Text>
                </Flex>
              </Flex>
            </Grid>
          </ModalBody>

          <ModalFooter>
            <Button
              bgColor="gray.800"
              colorScheme="teal"
              mr={3}
              onClick={onClose}
            >
              Close
            </Button>
            <Button
              onClick={goToCartHandler}
              colorScheme="teal"
              my="2"
              textTransform="uppercase"
              letterSpacing="wide"
              isDisabled={product.countInStock === 0}
            >
              Go to Cart
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CartModal;
