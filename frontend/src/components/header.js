import {
  Link,
  Box,
  Heading,
  Flex,
  Icon,
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { HiShoppingBag, HiUser, HiOutlineMenuAlt3 } from "react-icons/hi";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useState } from "react";
import { IoChevronDown } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../actions/userAction";
import { IoMdLogOut } from "react-icons/io";
import { CgProfile } from "react-icons/cg";

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/");
  };

  return (
    <Flex
      as="header"
      justify="space-between"
      wrap="wrap"
      bgColor="#000a35"
      py="5"
      px="5"
      pos="fixed"
      zIndex="999999"
      w="100%"
      top="0"
    >
      <Heading as="h1" size="md" color="#eef0ed">
        <Link
          href="/"
          textDecoration="none"
          _hover={{ textDecoration: "none", color: "white" }}
        >
          RST Store
        </Link>
      </Heading>

      <Box
        display={{ base: "block", md: "none", sm: "block" }}
        onClick={() => {
          setShow(!show);
        }}
      >
        <Icon as={HiOutlineMenuAlt3} color="white" mr="1" w="4" h="4" />
      </Box>

      <Box
        display={{ base: show ? "block" : "none", md: "flex" }}
        width={{ base: "full", md: "auto" }}
      >
        <Link
          as={RouterLink}
          to="/cart"
          fontWeight="bold"
          letterSpacing="wide"
          textTransform="uppercase"
          display="flex"
          alignItems="center"
          fontSize="md"
          color="white"
          mr="4"
          mt={{ base: 4, md: 0 }}
          _hover={{ color: "whiteAlpha.800" }}
        >
          <Icon as={HiShoppingBag} mr="1" w="4" h="4" />
          Cart
        </Link>
        {userInfo ? (
          <Menu>
            <MenuButton
              as={Button}
              variant="link"
              color="white"
              fontSize="lg"
              fontWeight="bold"
              rightIcon={<IoChevronDown />}
              _hover={{ textDecor: "none", opacity: "0.7" }}
            >
              <Flex justify="space-between" wrap="wrap">
                <Icon as={HiUser} m="1" w="4" h="4" />
                {userInfo.name}
              </Flex>
            </MenuButton>
            <MenuList>
              <MenuItem as={RouterLink} to="/profile">
                <Icon as={CgProfile} m="1" w="4" h="4" />
                Profile
              </MenuItem>
              <MenuItem onClick={logoutHandler}>
                <Icon as={IoMdLogOut} m="1" w="4" h="4" />
                Logout
              </MenuItem>
            </MenuList>
          </Menu>
        ) : (
          <Link
            as={RouterLink}
            to="/login"
            fontWeight="bold"
            textTransform="uppercase"
            display="flex"
            alignItems="center"
            fontSize="md"
            color="white"
            mr="4"
            mt={{ base: 4, md: 0 }}
            _hover={{ color: "whiteAlpha.800" }}
          >
            <Icon as={HiUser} mr="1" w="4" h="4" />
            Login
          </Link>
        )}

        {/* Admin Menu */}
        {userInfo && userInfo.isAdmin && (
          <Menu>
            <MenuButton
              as={Button}
              variant="link"
              color="white"
              fontSize="lg"
              fontWeight="bold"
              textTransform="uppercase"
              ml="3"
              _hover={{ textDecor: "none", opacity: "0.7" }}
            >
              Manage <Icon as={IoChevronDown} />
            </MenuButton>
            <MenuList>
              <MenuItem as={RouterLink} to="/admin/userlist">
                All Users
              </MenuItem>
              <MenuItem as={RouterLink} to="/admin/productlist">
                All Products
              </MenuItem>
              <MenuItem as={RouterLink} to="/admin/orderlist">
                All Orders
              </MenuItem>
            </MenuList>
          </Menu>
        )}
      </Box>
    </Flex>
  );
};

export default Header;
